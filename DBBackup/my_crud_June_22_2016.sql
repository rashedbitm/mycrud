-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 22, 2016 at 06:00 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `my_crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `delete_at` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `name`, `delete_at`) VALUES
(5, 'CSS', '1466071054'),
(6, 'JS', NULL),
(7, 'Bangla', NULL),
(8, 'English', NULL),
(9, 'Physics', NULL),
(11, 'Biology', NULL),
(12, 'Math', NULL),
(15, 'HTML 5', '1466061376'),
(16, 'CSS', NULL),
(17, 'Java 7', '1466071258'),
(19, 'Rashed', NULL),
(21, 'Rahim', '1466061007'),
(22, 'Shamim', NULL),
(23, 'Rashed', NULL),
(24, 'Rakib Hossain', '1466071253'),
(25, 'Rashed', NULL),
(26, 'Tech Bahgladdesh', '1466071247'),
(27, 'Delete Data', '1466071262'),
(28, 'Mainuddin Rashed', '1466071202'),
(29, 'Shahin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_name` varchar(20) NOT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_id`, `student_name`) VALUES
(1, ''),
(3, 'Mamun'),
(4, 'Yasin'),
(5, 'Tarek');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
