<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../resource/css/bootstrap.min.css">
    <script src="../../resource/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">Course Insert Panel</div>
        <div class="panel-body">
            <form role="form" method="post" action="store.php">
                <div class="form-group">
                    <label for="text">Course Name:</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter email">
                </div>

                <button type="submit" class="btn btn-default">Submit</button>
            </form>

        </div>
        <!-- Add Panel Footer Here -->
    </div>
</div>
</body>
</html>
