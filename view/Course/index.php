<?php
session_start();
include_once ('../../vendor/autoload.php');
use App\Course\Course;
use App\inc\Message;
//include_once ('../../controller/Course/Course.php');
$course = new course();
$allData = $course->index();
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../resource/css/bootstrap.min.css">
    <script src="../../resource/js/bootstrap.min.js"></script>
    <script src="../../resource/js/jquery.js"></script>
</head>
<body>
<div class="container">
    <h2>All Course List</h2> <a href="insert.php" class="btn btn-primary" role="button">Insert Again</a>
    <a href="trashed_view.php" class="btn btn-info" role="button">Trash List</a>
   <div id="message">
       <?php echo Message::message()?>
   </div>
    <form action="deleteMultiple.php" method="post" id="multiple">
        <input type="submit" value="Delete Selected" class="btn btn-info">
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th><input type="checkbox" id="checkAll"/></th>
                <th>#</th>
                <th>ID</th>
                <th>Book title</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allData as $course){
                $sl++; ?>
                <td><input type="checkbox" name=mark[] value="<?php echo $course->id; ?>"></td>
                <td><?php echo $sl;?></td>
                <td><?php echo $course->id;?></td>
                <td><?php echo $course->name ; ?></td>
                <td>
                    <a href="view.php?id=<?php echo $course->id; ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $course->id; ?>" class="btn btn-info" role="button">Edit</a>
                    <a href="doTrash.php?id=<?php echo $course->id; ?>" class="btn btn-danger" role="button">Delete</a>
                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
    </form>
</div>
<script>
    $('#message').show().delay(50000).fadeOut();
    //Delete Multiple Data using CheckBox
    $('#delete').on ('click',function() {
        document.forms['0'].action = "deleteMultiple.php";
        $('#multiple').submit();
    });
    // Below Code if For Select All
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });
</script>
</script>
</body>
</html>