<?php
use App\Course\Course;
include_once '../../vendor/autoload.php';
$course = new course();
$allData = $course->prepareData($_GET)->view();

//var_dump($_GET);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../resource/css/bootstrap.min.css">
   <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>-->
    <script src="../../resource/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="panel panel-info">
        <div class="panel-heading">Course Information <a href="index.php" class="btn-info">Go To DashBoard</a> </div>
        <div class="panel-body">
            <div class="list-group">
                <a href="#" class="list-group-item"><b>ID:</b> <?php echo $allData->id; ?></a>
                <a href="#" class="list-group-item"><b>Course Title:</b> <?php echo $allData->name; ?></a>
            </div>

        </div>
        <!-- Add Panel Footer Here -->
    </div>
</div>
</body>
</html>
