<?php
include_once ('../../vendor/autoload.php');
use App\Course\Course;
$course = new course();
$editSingleItem = $course->prepareData($_GET)->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Update Course</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../resource/css/bootstrap.min.css">
    <script src="../../resource/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">Update Insert Panel || <a href="index.php" class="btn-info">Go To DashBoard</a> </div>
        <div class="panel-body">
            <form role="form" method="post" action="update.php">
                <div class="form-group">
                    <input type="hidden" name="id"   value="<?php echo $editSingleItem->id;?>">
                    <label for="text">Course Name:</label>
                    <input type="text" class="form-control" name="name" value="<?php echo $editSingleItem->name; ?>">
                </div>
                <button type="submit" class="btn btn-default">Update</button>
            </form>

        </div>
        <!-- Add Panel Footer Here -->
    </div>
</div>
</body>
</html>
