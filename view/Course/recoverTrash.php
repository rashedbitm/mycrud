<?php
include_once ('../../vendor/autoload.php');

use App\Course\Course;

$course = new Course();

$course->prepareData($_GET)->recoverTrashed();