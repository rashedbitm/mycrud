<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../resource/css/bootstrap.css"/>
    <script src="../../resource/js/bootstrap.js"></script>
</head>
<body>

<div class="container">
    <h2>Student Input Form</h2>
    <form role="form" action="Store.php" method="post">
        <div class="form-group">
            <label for="text">Email:</label>
            <input type="text" name="student_name" class="form-control" id="student_name" placeholder="Enter Name">
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>