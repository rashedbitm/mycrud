<?php
/**
 * Created by PhpStorm.
 * User: Smart pc
 * Date: 6/12/2016
 * Time: 11:52 AM
 */
namespace App\Course;
use App\Inc\Message;

class Course {
        public $id="";
        public $name="";
        public $connection;
        public $delete_at;

        public function prepareData ($data=""){
            if (array_key_exists("name",$data )){
                 $this->name = $data['name'];
             }
            if(array_key_exists("id",$data )){
                $this->id = $data['id'];
            }
            return $this ;

            //return $this->id;
        }
        public function __construct()
        {
            $this->connection = mysqli_connect("localhost","root","","my_crud") or die("Database Not Connected");
        }

    public function insert (){
        $query = "INSERT INTO `my_crud`. `course` (`name`) VALUES ('".$this->name."')";
        $result = mysqli_query($this->connection,$query);
        if($result){
            Message::message('<div class="alert alert-success">
               <strong>Inserted!</strong> Data has been Inserted successfully.</div>');
            header('Location:index.php');
        }
        else
        {
            echo "Try again";
        }
    }

    public function index (){
        $all_data = array();
        $query= "SELECT * FROM `course` WHERE `delete_at` IS NULL";
        $result = mysqli_query($this->connection,$query);
        while($row= mysqli_fetch_object($result)){
            $all_data[]= $row;
        }
        return $all_data;
    }

    public function view(){
        $query = "SELECT * FROM `course` WHERE `id` =".$this->id;
        $result = mysqli_query($this->connection, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    public function update(){
        $query = "UPDATE `my_crud`.`course` SET `name` ='".$this->name."' WHERE `course`.`id`=".$this->id;
        $result = mysqli_query($this->connection, $query);
        if($result){
            Message::message('<div class="alert alert-success">
               <strong>Updated!</strong> Data has been Updated successfully.</div>');
            header('Location:index.php');

        }
        echo "Not Updated";
    }
    public function delete(){
        $query = "DELETE FROM `my_crud`.`course` WHERE `course`.`id` =".$this->id;
        $result = mysqli_query($this->connection, $query);
        if ($result){
            Message::message('<div class="alert alert-success">
               <strong>Inserted!</strong> Selected Data has been Deleted successfully.</div>');
            header('Location:trashed_view.php');

        }
        else{
            echo "Not Deleted";
        }
    }
    public function doTrash(){
        $this->delete_at=time();
        $query = "UPDATE `my_crud`.`course` SET `delete_at`='".$this->delete_at."'WHERE `course`.`id` =".$this->id;
        $result = mysqli_query($this->connection,$query);
        if($result){
            Message::message('<div class="alert alert-success">
               <strong>Deleted!</strong>  Data has been Trashed successfully.</div>');
            header('Location:index.php');
        }
        else
        {
            echo "Not trashed updated";
        }
    }
    public function getTrashed(){
        $getTrashed= array();
        $query= "SELECt * FROM `my_crud`.`course` WHERE `delete_at` IS NOT NULL";
        $result = mysqli_query($this->connection,$query);
        while ($row= mysqli_fetch_object($result)){
            $getTrashed[]=$row;
        }
        return $getTrashed;
    }
    public function recoverTrashed(){
        $query= "UPDATE `my_crud`.`course` SET `delete_at`= NULL WHERE `course`.`id` =".$this->id;
        //$query="UPDATE `atomicprojectb21`.`book` SET `deleted_at` = NULL  WHERE `book`.`id` = ".$this->id;
        $result = mysqli_query($this->connection, $query);
        if($result){
            Message::message('<div class="alert alert-success">
               <strong>Recovered!</strong> Data has been Recovered successfully.</div>');
            header('Location:index.php');
        }
        else
        {
            echo "Not Recovered Problem";
        }
    }
    public function recoverTrashedMultiple($recoverID=array()){
        if (is_array($recoverID)&& count ($recoverID)>0){
            $getRecover= implode(',',$recoverID );
            $query= "UPDATE `my_crud`.`course` SET `delete_at`= NULL WHERE `course`.`id` IN (".$getRecover.")";
            $result = mysqli_query($this->connection, $query);
            if($result){
                Message::message('<div class="alert alert-success">
               <strong>Recovered!</strong> Selected Data has been Recovered successfully.</div>');
                header('Location:index.php');
            }
            else
            {
                echo "Not Recovered Problem";
            }
        }
    }
    public function deleteMultiple($IDs =array()){
        if(is_array($IDs) && count($IDs)>0){
            $idS = implode(",",$IDs);
            $query = "DELETE FROM `my_crud`.`course` WHERE `course`.`id`IN(\".$idS.\")";
            //$query = "DELETE FROM `my_crud`.`course` WHERE `course`.`id`IN(\".$idS.\")";
            $result = mysqli_query($this->connection,$query);
            if ($result){
                Message::message('<div class="alert alert-success">
               <strong>Deleted!</strong> Selected Data has been Deleted successfully.</div>');
                header('Location:index.php');
            }
            else
            {
                Message::message("Data Not Deleted Successfully");
            }
        }
    }
}
