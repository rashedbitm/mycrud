<?php
/**
 * Created by PhpStorm.
 * User: Smart pc
 * Date: 6/15/2016
 * Time: 9:27 AM
 */

namespace App\Inc;
include_once ('../../vendor/autoload.php');
if(!isset($_SESSION['message'])){
    session_start();
}

class Message
{
    public static function message ($message=NULL){
        if (is_null($message)){
            $_message = self:: getMessage();
            return $_message;
        }
        else {
            self::setMessage($message);
        }
    }
    public static function setMessage($message){
        $_SESSION['message'] = $message;
    }
    public static function getMessage(){
        $_message = $_SESSION['message'];
        $_SESSION['message'] = "";
        return $_message;
    }
}